import {Routes, RouterModule} from "@angular/router" ;
import {LoginComponent} from "./login/login.component";
import {DashboardComponent} from "./dashboard/dashboard.component";
import {RegisterComponent} from "./register/register.component";

export const APP_ROUTES:Routes = [

  {
    path: 'login', component: LoginComponent
  },
  {
    path: 'dashboard', component: DashboardComponent
  },
  {
    path: 'register', component: RegisterComponent
  },
  {
    path: '', redirectTo: '/login', pathMatch: 'full'
  }


];

export const routing = RouterModule.forRoot(APP_ROUTES);
