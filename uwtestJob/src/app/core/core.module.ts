import {BrowserModule} from '@angular/platform-browser';
import {NgModule, Optional, SkipSelf} from '@angular/core';
import {HttpModule} from '@angular/http';

import {RestangularModule} from 'ng2-restangular';
import {RestServiceService} from "../service/rest-service.service";

export function restangular(RestangularProvider) {
  RestangularProvider.setBaseUrl('http://sample-env.rr2mmhwsmw.eu-west-1.elasticbeanstalk.com/api/');
}

@NgModule({
  declarations: [],
  imports: [
    BrowserModule,
    HttpModule,
    RestangularModule.forRoot(restangular
    ),
  ],
  providers: [RestServiceService],
})

export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule:CoreModule) {
    if (parentModule) {
      throw new Error(
        'CoreModule is already loaded. Import it in the AppModule only');
    }
  }
}
