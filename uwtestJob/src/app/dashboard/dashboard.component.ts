import {Component, OnInit} from '@angular/core';
import {RestServiceService} from "../service/rest-service.service";
import {LocalStorageService} from "../service/local-storage.service";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  msgs:any;
  newMsg:any;

  constructor(private restService:RestServiceService, private localStore:LocalStorageService) {
  }

  ngOnInit() {

    let msgs = this.restService.getMessages();
    msgs.subscribe(
      res => {
        console.log(res.data);
        this.msgs = res.data;
      },
      err => {
        console.log(err);
      });
  }

  addMsg() {

    let msg = this.restService.sendMessage(this.newMsg);
    msg.subscribe(
      res => {
        console.log(res.data);
        this.msgs.push(res.data);
        this.msgs[this.msgs.length - 1].user = {};
        console.log(this.localStore.getUserName(), this.msgs[this.msgs.length - 1].user.username);
        this.msgs[this.msgs.length - 1].user.username = this.localStore.getUserName();
        this.newMsg = "";
      },
      err => {
        console.log(err);
      });
  }
}
