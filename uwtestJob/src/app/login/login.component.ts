import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import {Router} from '@angular/router';
import {RestServiceService} from "../service/rest-service.service";
import {Restangular, RestangularHttp} from "ng2-restangular";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  constructor(private router:Router, private restService:RestServiceService) {
  }

  user = {
    username: '',
    password: ''
  };

  ngOnInit() {



  }

  login(user:NgForm) {
    // console.log("Login Method");
    // console.log(this.user.email == "admin@user.com");

    // this.router.navigate(['dashboard']);
    this.restService.login(this.user).subscribe(
      res => {
        console.log(res);
        this.router.navigate(['dashboard']);
      },
      err => {
        console.log(err);
      });
// console.log("valid user");
  }
}
