import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {RestServiceService} from "../service/rest-service.service";
import {Router} from '@angular/router';
import {Restangular} from "ng2-restangular/dist/esm/src/index";


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  user = {
    firstname: '',
    lastname: '',
    username: '',
    password: ''

  };

  constructor(private router:Router, private restService:RestServiceService, restangular:Restangular) {
  }

  ngOnInit() {
  }

  onSubmit(user:NgForm) {
    this.restService.register(this.user).subscribe(
      res => {
        console.log(res.data);
        this.router.navigate(['login']);
      },
      err => {
        console.log(err);
      });
    console.log(user);
  }
  
}
