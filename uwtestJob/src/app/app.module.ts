import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {RouterModule} from '@angular/router';

import {AppComponent} from './app.component';
import {LoginComponent} from './login/login.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {Restangular, RestangularHttp} from "ng2-restangular/dist/esm/src/index";
import {LocalStorageService} from "./service/local-storage.service";
import {routing} from "./app.router";
import {CoreModule} from "./core/core.module";
import { RegisterComponent } from './register/register.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    RegisterComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing,
    CoreModule
  ],
  providers: [RouterModule, Restangular, RestangularHttp, LocalStorageService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
