import {Injectable} from '@angular/core';

@Injectable()
export class LocalStorageService {

  constructor() {
  }

  addTokenInStorage(token:any) {

    localStorage.setItem('token', token);

  }

  checkForToken() {

    return localStorage.getItem('token') ? true : false;
  }

  getToken() {
    return localStorage.getItem('token');
  }

  saveUser(user)
  {
    localStorage.setItem('user',user._id);
  }

  getUser() {
    return localStorage.getItem('user');
  }

  saveUserName(user)
  {
    localStorage.setItem('username',user.username);
  }

  getUserName() {
    return localStorage.getItem('username');
  }


}
