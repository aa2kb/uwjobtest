import {Injectable} from '@angular/core';
import {Restangular} from "ng2-restangular";
import {LocalStorageService} from "./local-storage.service";

@Injectable()
export class RestServiceService {

  // pro:prospects;

  constructor(private restangular:Restangular, private localStore:LocalStorageService) {
    // pro = new prospects(restangular);
  }

  login(data:any) {
    let user = this.restangular.one('users').one('login').customPOST(data);
    user.subscribe(
      res => {
        console.log(res.data);
        this.localStore.saveUser(res.data.user);
        this.localStore.saveUserName(res.data.user);
        this.localStore.addTokenInStorage(res.data.token);
        this.restangular.provider.setDefaultHeaders({'x-access-token': res.data.token});
        // console.log(this.restangular.provider);
      },
      err => {
        console.log(err);
      });
    console.log(user);
    return user;
  }
  register(data:any) {
    let user = this.restangular.one('users').one('register').customPOST(data);
    user.subscribe(
      res => {
        console.log(res.data);
      },
      err => {
        console.log(err);
      });
    console.log(user);

    return user;
  }

  getMessages() {
    this.restangular.provider.setDefaultHeaders({'x-access-token': this.localStore.getToken()});
    return this.restangular.one('messages').get();

  }

  sendMessage(data) {
    console.log(data);
    let newmsg = {
      msg : data,
      userId : this.localStore.getUser()
    };
    this.restangular.provider.setDefaultHeaders({'x-access-token': this.localStore.getToken()});
    return this.restangular.one('messages').customPOST(newmsg);
  }

}
