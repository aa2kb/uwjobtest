import { UwtestJobPage } from './app.po';

describe('uwtest-job App', function() {
  let page: UwtestJobPage;

  beforeEach(() => {
    page = new UwtestJobPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
