var express = require('express');
var router = express.Router();
var msgCtrl = require('./msg.controller.js');
var log = require('tracer').console({format: "{{message}}  - {{file}}:{{line}}"}).log;
var verify = require('../../server/verify');

/**
 * @api {get} api/messages List All messages
 * @apiVersion 0.1.0
 * @apiName List All Messages
 * @apiGroup Messages
 *  * @apiDescription This api is used to get all the messages in the record.
 *
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
*          message: 'All Messages',
            success: true,
            data: All Messages
*     }
 */

router.get('/', verify.user, msgCtrl.listAll);


/**
 * @api {post} api/messages Add New message
 * @apiVersion 0.1.0
 * @apiName Add message
 * @apiGroup Messages
 *  * @apiDescription This api is used to add message into the app.
 *  user id and message should be given in body.
 *  It would return message and user id.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
*         message: 'Message saved',
            success: true,
            data: Message
*     }
 */
router.post('/', msgCtrl.addMessage);

module.exports = router;