var log = require('tracer').console({format: "{{message}}  - {{file}}:{{line}}"}).log;
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Message = new Schema({
    message: String,
    user: {
        type: Schema.Types.ObjectId, ref: 'User'
    }
});


module.exports = mongoose.model('Message', Message);