var Message = require('./msg.model.js');
var passport = require('passport');
var Verify = require('../../server/verify.js');
var log = require('tracer').console({format: "{{message}}  - {{file}}:{{line}}"}).log;
var auth = require('../../server/auth');

exports.listAll = function (req, res, next) {
    Message.find({})
        .populate('user')
        .exec(function (err, msgs) {
        if (err) {
            return res.status(500).json({
                message: 'Something went wrong while getting Messages',
                success: false,
                data: null
            });
        }
        res.status(200).json({
            message: 'All Messages',
            success: true,
            data: msgs
        });
    });
};

exports.addMessage = function (req, res, next) {
    log(req.body);
    var message = new Message({
        message: req.body.msg,
        user: req.body.userId
    });

    message.save(function (err, Message) {
        if (err) {
            return res.status(500).json({
                message: 'Something went wrong while saving new Message',
                success: false,
                data: null
            });
        }
        res.status(200).json({
            message: 'Message saved',
            success: true,
            data: Message
        });
    })
};
