var express = require('express');
var router = express.Router();
var userCtrl = require('./user.controller.js');
var log = require('tracer').console({format: "{{message}}  - {{file}}:{{line}}"}).log;
var verify = require('../../server/verify');

//GET users
router.get('/', verify.user, userCtrl.listAll);

/**
 * @api {post} api/users/register Register
 * @apiVersion 0.1.0
 * @apiName Register
 * @apiGroup User
 *  * @apiDescription This api is used to register user into the app.
 *  User firstname, lastname, username and password should be given in body.
 *  If username already exists server return with an error. Username should be unique.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        "message": "User registered",
 *        "success": true,
 *        "data": null
 *     }
 */
router.post('/register', userCtrl.register);

/**
 * @api {post} api/users/login Login
 * @apiVersion 0.1.0
 * @apiName Login
 * @apiGroup User
 *  * @apiDescription This api is used to login user into the app.
 *  User username and password should be given in body.
 *  It would return user object and token
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
*         message: 'Login successful!',
          success: true,
          data: data
*     }
 */
router.post('/login', userCtrl.login);

module.exports = router;