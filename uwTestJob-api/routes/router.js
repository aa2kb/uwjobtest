var express = require('express');
var users =  require('../features/users/user.router');
var messages =  require('../features/messages/msg.router');
var log = require('tracer').console({format : "{{message}}  - {{file}}:{{line}}"}).log;
var verify = require('../server/verify');

module.exports = function (app, config, models) {
  var router = express.Router();

  router.use('/users',users);
  router.use('/messages',messages);




  app.use('/api', router);
};
