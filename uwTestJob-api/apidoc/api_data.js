define({ "api": [
  {
    "type": "post",
    "url": "api/messages",
    "title": "Add New message",
    "version": "0.1.0",
    "name": "Add_message",
    "group": "Messages",
    "description": "<p>This api is used to add message into the app. user id and message should be given in body. It would return message and user id.</p>",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    message: 'Message saved',\n        success: true,\n        data: Message\n}",
          "type": "json"
        }
      ]
    },
    "filename": "features/messages/msg.router.js",
    "groupTitle": "Messages"
  },
  {
    "type": "get",
    "url": "api/messages",
    "title": "List All messages",
    "version": "0.1.0",
    "name": "List_All_Messages",
    "group": "Messages",
    "description": "<p>This api is used to get all the messages in the record.</p>",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     message: 'All Messages',\n        success: true,\n        data: All Messages\n}",
          "type": "json"
        }
      ]
    },
    "filename": "features/messages/msg.router.js",
    "groupTitle": "Messages"
  },
  {
    "type": "post",
    "url": "api/users/login",
    "title": "Login",
    "version": "0.1.0",
    "name": "Login",
    "group": "User",
    "description": "<p>This api is used to login user into the app. User username and password should be given in body. It would return user object and token</p>",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    message: 'Login successful!',\n      success: true,\n      data: data\n}",
          "type": "json"
        }
      ]
    },
    "filename": "features/users/user.router.js",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "api/users/register",
    "title": "Register",
    "version": "0.1.0",
    "name": "Register",
    "group": "User",
    "description": "<p>This api is used to register user into the app. User firstname, lastname, username and password should be given in body. If username already exists server return with an error. Username should be unique.</p>",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   \"message\": \"User registered\",\n   \"success\": true,\n   \"data\": null\n}",
          "type": "json"
        }
      ]
    },
    "filename": "features/users/user.router.js",
    "groupTitle": "User"
  }
] });
